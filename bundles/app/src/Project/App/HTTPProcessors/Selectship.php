<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 03.01.2017
 * Time: 17:24
 */

namespace Project\App\HTTPProcessors;


use Project\App\HTTPProcessors\Processor\UserProtected;

class Selectship extends UserProtected
{
    /**
     * The Builder will be used to access
     * various parts of the framework later on
     * @var Project\App\HTTPProcessors\Builder
     */
    protected $builder;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    // This is the default action

    public function defaultAction(Request $request)
    {
        $raccoon = ['raccoon' => 'Chemax',
            'raccoon2' => 'Nina'];
        $template = $this->builder->components()->template();
        return $template->render('app:quickstart/message', array(
            'user' => $this->user,
            'raccoon' => $raccoon,
            'message' => "hello, raccoon"
        ));
    }
}
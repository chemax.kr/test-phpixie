<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 02.01.2017
 * Time: 14:07
 */
namespace Project\App\HTTPProcessors;

use PHPixie\HTTP\Request;
use Project\App\Auth;
use Project\App\HTTPProcessors\Processor\UserProtected;
use Project\App\ORM\User\User;


class Quickstart extends UserProtected// extends \PHPixie\DefaultBundle\Processor\HTTP\Actions
{
    /**
     * The Builder will be used to access
     * various parts of the framework later on
     * @var Project\App\HTTPProcessors\Builder
     */
    protected $builder;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    // This is the default action

    public function defaultAction(Request $request)
    {
        $raccoon = ['raccoon' => 'Chemax',
            'raccoon2' => 'Nina'];
        $template = $this->builder->components()->template();
        return $template->render('app:quickstart/message', array(
            'user' => $this->user,
            'raccoon' => $raccoon,
            'message' => "hello, raccoon"
        ));
    }

//    public function defaultAction(Request $request)
//    {
////        return $this->components->template()->get('app:quickstart/layout', array(
////            'user' => $this->user
////        ));
//        $raccoon = ['raccoon' => 'Chemax',
//            'raccoon2' => 'Nina'];
//        return $this->components->template()->get('app:user/dashboard', array(
//            'user' => $this->user,
//            'raccoon' => $raccoon
//        ));
////        return "Quickstart tutorial";
//    }

    public function viewAction(Request $request)
    {
        //Output the 'id' parameter
        return $request->attributes()->get('id');
    }

    public function renderAction(Request $request)
    {
        $template = $this->builder->components()->template();
        $user = new UserProtected();
        var_dump($user);
        return $template->render(
            'app:quickstart/message',
            array(
                'message' => 'hello'
            )
        );
    }

    public function ormAction(Request $request)
    {
        $orm = $this->builder->components()->orm();

        $projects = $orm->query('project')->find();

        //Convert enttities to simple PHP objects
        return $projects->asArray(true);
    }
}